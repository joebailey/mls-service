FROM node:12.4.0-alpine as base

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
FROM base AS dependencies
COPY package*.json ./
#install prod dependencies
RUN npm install --only=production --no-optional
# copy production node_modules aside
RUN cp -R node_modules prod_node_modules
RUN npm install

FROM dependencies as build
COPY . .
RUN npm ci

FROM base AS release
COPY --from=dependencies /usr/src/app/prod_node_modules ./node_modules
COPY --from=build /usr/src/app/src ./src
COPY --from=build /usr/src/app/.env.* ./
COPY ./package.json .
RUN npm install
EXPOSE 80/tcp
ENV PORT=80
CMD ["npm", "start"]
