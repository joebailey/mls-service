let nodeEnv = 'production';

try {
    const defaultEnv = require('dotenv').config({path: `.env`});

    if (defaultEnv != null || defaultEnv.parsed != null)
    {
        nodeEnv = defaultEnv.parsed.NODE_ENV ? 'production' : defaultEnv.parsed.NODE_ENV;
    }
}
catch (e) {
    console.log(e.message);
}

const env = require('dotenv').config({path: `.env.${nodeEnv}`});

module.exports = env.parsed;
