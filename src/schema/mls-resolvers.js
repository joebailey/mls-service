const { Client } = require('@elastic/elasticsearch');
const env = require('../env');

const excludeStatuses = [ "Cancelled", "Canceled", "Expired", "Withdrawn", "Withdrawl", "Withdrawal" ];

const esClient = new Client({
  node: env.SEARCH_ES_TARGET
})

const SEARCH_INDEX_NAME = env.SEARCH_ES_INDEX;

const listingReducer = (listing) => {
  return {
    yearBuilt: listing._source.year_built,
    propertyId: listing._source.property_id,
    listDate: listing._source.list_date,
    listingPrice: listing._source.listing_price,
    listingStatus: listing._source.listing_status,
    listingAgentSummaryInfo: {
      name: listing._source.agent_name,
    },
    propertyAddress: {
      city: listing._source.city,
      zipCode: listing._source.zip_code
    },
    displayAddress: listing._source.display_address,
    displayMlsNumber: listing._source.display_mlsnumber,
    marketName: listing._source.market_name,
    lastUpdateDate: listing._source.last_update_date,
    photoUrl: listing._source.photo_url
  }
}

const MLSResolvers = {
  Query: {
    getListingsByMlsNumber: async (parent, { mlsNumber }) => {
      const result = await esClient.search({
        index: SEARCH_INDEX_NAME,
        body: {
          query: {
            bool: {
              must: [
                { match: { _isdeleted: false }},
                { match: { _issearchable: true }},
                { match: { display_mlsnumber: mlsNumber } }
              ],
              must_not: {
                terms: {
                  listing_status: excludeStatuses
                }
              }
            }
          }
        }
      });

      const hits = result.body.hits.hits;
      console.log('result', result);
      console.log('hits', hits);

      return Array.isArray(hits)
      ? hits.map(listing => listingReducer(listing))
      : [];
    },

    getListingsByAgentName: async (parent, { agentName }) => {
      const result = await esClient.search({
        index: SEARCH_INDEX_NAME,
        body: {
          query: {
            bool: {
              must: [
                { match: { _isdeleted: false }},
                { match: { _issearchable: true }},
                { match: { agent_name: agentName } }
              ],
              must_not: {
                terms: {
                  listing_status: excludeStatuses
                }
              }
            }
          }
        }
      });

      const hits = result.body.hits.hits;
      console.log('result', result);
      console.log('hits', hits);

      return Array.isArray(hits)
      ? hits.map(listing => listingReducer(listing))
      : [];
    },

    getListingsByAddress: async (parent, {streetNumber = '', streetName = '', city = '', zipCode = ''}) => {
      const result = await esClient.search({
        index: SEARCH_INDEX_NAME,
        sort: ['_score: desc', 'list_date: desc'],
        body: {
          query: {
            bool: {
              should: [
                { match: { display_address: streetNumber } },
                { match: { display_address: streetName } },
                { match: { city: city } },
                { match: { zip_code: zipCode } },
              ],
              must: [
                { match: { _isdeleted: false } },
                { match: { _issearchable: true } },
              ],
              must_not: {
                terms: {
                  listing_status: excludeStatuses
                }
              },
              minimum_should_match: 1
            }
          }
        }
      });

      const hits = result.body.hits.hits;
      console.log('result', result);
      console.log('hits', hits);

      return Array.isArray(hits)
      ? hits.map(listing => listingReducer(listing))
      : [];
    },
    
    healthCheck: async (parent, args) => {
      try {
        const result = await esClient.ping();

        console.log('Healtcheck passed!', result);
        return {
          isHealthy: true,
          statusCode: result.statusCode,
          targetName: result.meta.connection.url,
          dateChecked: new Date(),
          message: 'Heathcheck passed!',
          details: ''
        }
      } catch (ex) {
        console.log('Heathcheck failed!', ex);
        console.log('meta connection', ex.meta.meta.connection);
        console.log('meta request', ex.meta.meta.request);
        return {
          isHealthy: false,
          statusCode: 500,
          targetName: '',
          dateChecked: new Date(),
          message: 'Heathcheck failed!',
          details: ex
        }
      }
    }
  }
}

module.exports = MLSResolvers;
