const { gql } = require('apollo-server');

const MLSQueries = gql`
  type Query {
    healthCheck: HealthCheckResponse
    getListingsByMlsNumber(mlsNumber: String): [Listing]
    getListingsByAddress(streetNumber: String, streetName: String, city: String, zipCode: String): [Listing]
    getListingsByAgentName(agentName: String): [Listing]
  }
`;

module.exports = MLSQueries;