const { gql } = require('apollo-server');

const MLSTypeDefs = gql`
  type HealthCheckResponse {
    isHealthy: Boolean!
    statusCode: Int
    targetName: String
    message: String
    details: String
    dateChecked: DateTime
  }

  type Listing {
    id: ID!
    propertyId: String!
    propertySubType: String
    propertyType: String
    propertyTypeCode: String
    propertyTypeStandardized: String
    propertyAddress: Address
    marketName: String
    yearBuilt: PositiveInt

    listingAgentSummaryInfo: AgentSummaryInfo!
    coListingAgentSummaryInfo: AgentSummaryInfo
    sellingAgentSummaryInfo: AgentSummaryInfo
    coSellingAgentSummaryInfo: AgentSummaryInfo
    
    originalListingPrice: USCurrency
    listingPrice: USCurrency
    soldPrice: USCurrency

    listingStatus: String
    listingStatusStandardized: String
    photoUrl: String
    thumbnailUrl: String
    virtualTourUrl: String
    virtualTourBrandedUrl: String


    displayAddress: String
    displayLatitude: Float
    displayLongitude: Float
    displayMlsNumber: String
    Latitude: Float
    Longitude: Float

    buildingName: String
    complexName: String
    community: String
    neighborhood: String
    subDivision: String
    township: String
    lakeName: String
    elementarySchool: String
    middleSchool: String
    jrHighSchool: String
    highSchool: String
    schoolDistrictId: Int
    schoolDistrictName: String

    cancelledDate: DateTime
    closeDate: DateTime
    contractDate: DateTime
    expirationDate: DateTime
    lastUpdateDate: DateTime
    listDate: DateTime
    offMarketDate: DateTime
    originalListDate: DateTime
    pendingDate: DateTime
    soldDate: DateTime
    tempOffMarketDate: DateTime

    mlsCreateDate: DateTime
    mlsListDate: DateTime
    mlsUpdateDate: DateTime
    mlsGeoCodeEquality: DateTime
    mlsId: String
    mlsLatitude: Float
    mlsLongitude: Float
    mlsOriginatingSystemName: String

    geoCodeDate: DateTime
    geoCodeEquality: String

    allowAVM: YesNo
    allowIDX: YesNo
    hideAddress: YesNo
    hasFamilyRoom: YesNo
    hasFireplace: YesNo
    hasBasement: YesNo
    hasGarage: YesNo
    hasGolf: YesNo
    hasHorseProperty: YesNo
    hasLakefront: YesNo
    hasMountainView: YesNo
    hasOpenHouse: YesNo
    hasPool: YesNo
    hasWaterfront: YesNo
    hasWaterview: YesNo
    hasPropertyView: YesNo
    hasHoa: YesNo
    isGatedCommunity: YesNo
    isGolfCommunity: YesNo
    isAdultCommunity: YesNo
    isBoatSlip: YesNo
    isBusinessWithRealEstate: YesNo
    isCommercial: YesNo
    isCommercialLease: YesNo
    isCondo: YesNo
    isCondoTownhouse: YesNo
    isCulDeSac: YesNo
    isDuplex: YesNo
    isFarmHobby: YesNo
    isForeclosure: YesNo
    isHalfDuplex: YesNo
    isIndustrial: YesNo
    isInvestment: YesNo
    isLoft: YesNo
    isLot: YesNo
    isMixedUse: YesNo
    isMobileHome: YesNo
    isModel: YesNo
    isMultiFamily: YesNo
    isRental: YesNo
    isResidentialLease: YesNo
    isResidentialLeaseDetached: YesNo
    isRetailStore: YesNo
    isShortSale: YesNo
    isSingleFamily: YesNo
    isSingleFamilyDetached: YesNo
    isTownHouse: YesNo
    isNewConstruction: YesNo
    isOnGolfCourse: YesNo
    isOneStory: YesNo
    isThreePlusStory: YesNo
    isPriceReduced: YesNo
    isCoop: YesNo
    isOther: YesNo

    acres: Float
    acresRange: String
    squareFeet: Int
    squareFeedRange: String
    lotDimension: String
    areaInt: Int
    areaName: String

    daysOnMarket: String
    annualTax: Float
    approximateAge: Int
    downPaymentResource: String
    foreclosureType: String
    garage: String
    garageSpaces: Int
    numOfParkingSpaces: Int
    style: String
    totalBathrooms: Float
    totalBedrooms: Int
    totalFireplaces: Int
    totalFullBaths: Int
    totalHalfBaths: Int
    totalPartialBaths: Int
    totalStories: Int
    waterfront: String
  }

  type Address {
    streetNumber: String
    streetName: String
    streetDirection: String
    streetType: String
    unitNumber: String
    city: String
    state: String
    stateProvinceFullname: String
    zipCode: String
    county: String
    countryCode: String
  }

  type AgentSummaryInfo {
    agentId: String
    name: String
    phone: String
    email: String
    officeId: String
    officeName: String
    officePhone: String
  }

  type Agent {
    agentId: String!
    officeId: String
    nrdsId: String
    licenseNumber: String
    type: String
    lastUpdateDate: DateTime
    fullName: String
    firstName: String
    middleName: String
    lastName: String
    userName: String
    email: String
    preferredPhone: String
    cellPhone: String
    tollFreePhone: String
    website: String
    blogAddress: String
    photoUrl: String
    facebookUrl: String
    preferredLanguage: String
    primaryAssociation: String
    designations: String
    status: String
    statusDate: String
    sourceModificationTimestamp: String
    modificationTimestamp: String
    systemLocale: String
    subSystemLocale: String
    key: String
    moKey: String
    sourceCreationTimestamp: String
    countryCode: String
    luxuryListingsFlag: YesNo
    commercialListingsFlag: YesNo
    mobileCarrier: String
    biography: String
    mlsId: String
    mlsCreateDate: DateTime
    mlsUpdateDate: DateTime
    wolfnetCreateDate: DateTime
  }

  type Office {
    lastUpdateDate: DateTime
    officeId: String!
    nrdsId: String
    fullName: String
    shortName: String
    email: String
    website: String
    phone: String
    address: String
    city: String
    state: String
    zipCode: String
    zipCode4: String
    countryCode: String
    stateProvinceServiceArea: String
    contactFirstName: String
    contactLastName: String
    contactEmail: String
    contactPhone: String
    brokerId: String
    brokerFirstName: String
    brokerLastName: String
    brokerFullName: String
    brokerEmail: String
    brokerPhone: String
    brokerLogoText: String
    primaryAssociation: String
    type: String
    status: String
    statusDate: String
    sourceCreationTimestamp: String
    sourceModificationTimestamp: String
    modificationTimestamp: String
    systemLocale: String
    subSystemLocale: String
    key: String
    parentOrgId: String
    minLuxuryPrice: String
    wolfnetCreateDate: DateTime
    mlsId: String
    mlsCreateDate: DateTime
    mlsUpdateDate: DateTime
  }

  type Photos {
    propertyId: String!
    photoFilename: String
    photoUrl: String
    thumbnailUrl: String
    caption: String
    displayOrder: Int
    photoCategory: String
    photoDate: DateTime
    lastUpdateDate: DateTime
    mlsUpdateDate: DateTime
    mlsId: String
  }
`;

module.exports = MLSTypeDefs;