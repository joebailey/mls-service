const { gql } = require('apollo-server');

const MLSEnums = gql`
  enum YesNo {
    Y
    N
  }
`;

module.exports = MLSEnums;