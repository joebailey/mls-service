const { ApolloServer } = require('apollo-server');
const { makeExecutableSchema } = require('graphql-tools');
const MLSEnums = require('./schema/mls-enums');
const MLSTypeDefs = require('./schema/mls-typedefs');
const MLSQueries = require('./schema/mls-queries');
const MLSResolvers = require('./schema/mls-resolvers');

const {
  DateTimeResolver, DateTimeTypeDefinition,
  PositiveIntResolver, PositiveIntTypeDefinition,
  EmailAddressResolver, EmailAddressTypeDefinition,
  URLResolver, URLTypeDefinition,
  USCurrencyResolver, USCurrencyDefinition
} = require('graphql-scalars');

const server = new ApolloServer({
  schema: makeExecutableSchema({
    typeDefs: [
      MLSEnums,
      MLSTypeDefs,
      MLSQueries,
      DateTimeTypeDefinition,
      PositiveIntTypeDefinition,
      EmailAddressTypeDefinition,
      URLTypeDefinition,
      USCurrencyDefinition
    ],
    resolvers: {
      ...MLSResolvers,
      DateTime: DateTimeResolver,
      PositiveInt: PositiveIntResolver,
      EmailAddress: EmailAddressResolver,
      URL: URLResolver,
      USCurrency: USCurrencyResolver,
    }
  })
});

server.listen().then(({ url }) => {
  console.log(`SkySlope MLS service is ready at: ${url}`);
});